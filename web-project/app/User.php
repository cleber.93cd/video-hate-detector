<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function annotations() {
        return $this->hasMany('App\Annotation');
    }
    
    public function sendPasswordResetNotification($token) {
        $url = url(url('/').route('password.reset', $token, false));
        Log::info('Password reset for ' . $this->email. ': '. $url);
    }
    
    public function firstName(){
        return explode(' ', $this->name)[0];
    }
}
