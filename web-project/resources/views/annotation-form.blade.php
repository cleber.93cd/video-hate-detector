@extends('layouts.contentpage')

@section('title')
{{trans('general.new_annotation')}}
@endsection

@section('content')
<div class="container mtb">
@if($video === NULL || empty($video->getMetadata()))
    <div class='row'>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="alert alert-info" role="alert">
                <i class='fa fa-exclamation-triangle'></i><strong> {{trans('general.no_video_available')}}</strong><br/>
                {{trans('general.no_video_available_text')}}
                <div class="text-center hidden-xs" style="font-size: 100pt">
                    <i class='fa fa-film'></i>
                    <i class='fa fa-clock-o'></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
@else
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <h4><i class='fa fa-film'></i> {{ucwords(trans_choice('general.video',1)) . ': ' . $video->id}}</h4>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-warning" style='border: 0px;' data-toggle="tooltip" data-placement="bottom" title="{{trans('general.remaining_time_tooltip')}}">
                            <i class="fa fa-clock-o"></i>
                            <span class="hidden-xs">{{trans('general.remaining_time')}}:</span>
                            <b><span id='countdownCounter'>-</span></b>
                        </button>
                        <button class="btn btn-theme" style='border: 0px;' data-toggle="modal" data-target="#instructionsModal">
                            <i class="fa fa-question-circle"></i> {{trans('general.instructions')}}
                        </button>
                        @include('partials.annotation-instructions')
                    </div>
                </div>
            </div>
            <div class="hline"></div>
            <p></p>
        </div>
        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
            <video class='img-responsive center-block' id='video' controls style="width: 100%;">
                @foreach($video->getMetadata() as $metadata)
                <source src="{{$metadata->url}}" type="{{$metadata->type}}">
                @endforeach
                <p>Your browser doesn't support HTML5 video.</p>
            </video>
            <div class='btn-group btn-group-justified' role='group' aria-label='Video Controllers'>
                <div class='btn-group'>
                    <button type="button" id='playButton' onclick="playPause()" class="btn btn-default btn-flat">
                        <i class="fa fa-play"></i><br/> <span>{{trans('general.player_play')}}</span>
                    </button>
                </div>
                <div class='btn-group'>
                    <button type="button" onclick="tag()" class="btn btn-danger btn-flat full-height"  data-toggle="tooltip" data-placement="bottom" title="{{trans('general.tag_tooltip')}}">
                        <i class="fa fa-tag"></i><br/> <span>{!!trans('general.tag_moment')!!}</span>
                    </button>
                </div>
            </div>
            <form action="{{Helpers::url('annotations/new')}}" id='tagForm'>
                <input type='hidden' name='tags' value=''>
                <input type='hidden' name='video_id' value='{{$video->id}}'>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <h4><i class='fa fa-tags'></i> {{trans('general.offensive_moments')}}</h4>
            <div class="hline"></div>
            <p></p>
            <div id='offensiveMoments'>
                <div class='form-group has-error' id='videoNotOffensiveButton' style="padding: 2px;">
                    <input type="checkbox"
                           name="isNotOffensive"
                           onchange='changeIsNotOffensive(this)'
                           data-toggle="toggle" 
                           data-on='<i class="fa fa-check"></i> {{trans('general.not_offensive')}}'
                           data-off='{{trans('general.not_offensive')}}'
                           data-onstyle='success' data-width='100%'>
                    <span class="help-block hidden">
                        {{trans('general.not_offensive_not_checked')}}
                    </span>
                </div>
                <div class="btn-group hidden" style="margin: 2px;" id='videoTagTemplate'>
                    <button type="button" class="btn btn-danger" onclick='removeTag(this)'><i class='fa fa-trash-o'></i></button>
                    <button type="button" class="btn btn-default" onclick='setVideoMoment(this)'><span class='video-time-tag'></span></button>
                </div>
            </div>
            <p></p>
            <button type="button" onclick="save()" class="btn btn-primary btn-block" data-toggle="tooltip" data-placement="bottom" title="{{trans('general.save_tooltip')}}">
                <i class="fa fa-save"></i> {{trans('general.save')}}
            </button>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
@endif
            <h4><i class='fa fa-gear'></i> {{trans('general.settings')}}</h4>
            <div class='hline'></div>
            <p></p>
            <form method='GET' action='{{Helpers::url('annotations/new')}}'>
                <div class='form-group'>
                    <label for='video_language'>{{trans('general.video_language')}}:</label>
                    <select class='form-control' id='video_language' name='video_language'>
                        <option value='pt' {{$video_language === 'pt'? 'selected' : ''}}>Português (PT)</option>
                        <option value='en' {{$video_language === 'en'? 'selected' : ''}}>English (EN)</option>
                    </select>
                    @if($video !== NULL && !empty($video->getMetadata()))
                    <p></p>
                    <div class="alert alert-warning" role="alert">
                        <i class='fa fa-exclamation-triangle'></i><strong> {{trans('general.attention')}}</strong><br/>
                        {{trans('general.settings_change_alert')}}
                    </div>
                    @endif
                </div>
                <button type='submit' class='btn btn-theme pull-right'><i class='fa fa-save'></i> {{trans('general.save_settings')}}</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-top-left-radius: 6px;border-top-right-radius: 6px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="confirmationModalLabel">Title</h4>
            </div>
            <div class="modal-body">
                Body
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('general.stop_annotation')}}</button>
                <a class="btn btn-theme" style='margin-top: 0px;' href="{{url()->full()}}">{{trans('general.keep_annotation')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
    var timeTags = [];
    var cookieName = 'instructionsShown';
    var video = $('#video')[0];
    var videoDuration = {!!$video? $video->duration : 0!!};
    var videoLockFactor = {!!config('vhd.video_lock_factor')!!};
    
    if(typeof video !== 'undefined'){
        video.onplay = play;
        video.onpause = pause;
    }

    window.onload = function () {
      var cookie = getCookie(cookieName);
      if (cookie !== 'true') {
        $('#instructionsModal').modal('show');
        document.cookie = cookieName + '=' + true;
      }
      if(videoDuration > 0){
        setTimer(videoDuration * videoLockFactor);
      }
    };
    function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return '';
    }

    function play() {
      $('#playButton').children('i').removeClass('fa-play').addClass('fa-pause');
      $('#playButton').children('span').text({!! json_encode(trans('general.player_pause')) !!});
    }

    function pause() {
      $('#playButton').children('i').removeClass('fa-pause').addClass('fa-play');
      $('#playButton').children('span').text({!! json_encode(trans('general.player_play')) !!});
    }

    function playPause() {
      if (video.paused) {
        video.play();
        play();
      } else {
        video.pause();
        pause();
      }
    }

    function tag() {
      var time = Math.floor(video.currentTime);
      if(timeTags.indexOf(time) === -1){
          if(timeTags.length === 0){
              $('#videoNotOffensiveButton').addClass('hidden');
          }
          timeTags.push(time);
        var element = $('#videoTagTemplate').clone(true).removeAttr('id').removeClass('hidden');
        setTagTime(element, time);
        element.appendTo('#offensiveMoments');
      }
    }
    
    function setTagTime(buttonGroupElement, seconds){
        var date = new Date(null);
        date.setSeconds(seconds);
        var timeString = date.toISOString().substr(11, 8);        
        buttonGroupElement.find('span.video-time-tag').text(timeString);
    }
    
    function getTagTime(buttonGroupElement){
        var isoTime = buttonGroupElement.find('span.video-time-tag').text();
        var isoTimeParts = isoTime.split(':');
        return parseInt(isoTimeParts[0])*60*60
                + parseInt(isoTimeParts[1])*60
                + parseInt(isoTimeParts[2]);
    }
    
    function removeTag(element){
        var buttonGroup = $(element).parent();
        var timeTag = getTagTime(buttonGroup);
        var indexOf = timeTags.indexOf(timeTag);
        
        buttonGroup.remove();
        
        if(indexOf !== -1){
            timeTags.splice(indexOf, 1);
        }
        
        if(timeTags.length === 0){
            $('#videoNotOffensiveButton').removeClass('hidden');
        }
    }
    
    function setVideoMoment(element){
        var buttonGroup = $(element).parent();
        var timeTag = getTagTime(buttonGroup);
        if(timeTag >= 0 && timeTag <= video.duration){
            video.currentTime = timeTag;
        }
    }
    
    function changeIsNotOffensive(element){
        var offensiveMomentsDiv = $('#offensiveMoments');
        if($(element).prop('checked')){
            offensiveMomentsDiv.find('.help-block').addClass('hidden');
        }
    }
    
    function save() {
        var isNotOffensiveChecked = $('input[name=isNotOffensive]').prop('checked');
        var offensiveMomentsDiv = $('#offensiveMoments');
        if(isNotOffensiveChecked){
            offensiveMomentsDiv.removeClass('has-error');
            offensiveMomentsDiv.find('.help-block').addClass('hidden');
        } else if(timeTags.length === 0){
            offensiveMomentsDiv.addClass('has-error');
            offensiveMomentsDiv.find('.help-block').removeClass('hidden');
            return;
        }

        var form = $('#tagForm');
        
        form.find('input[name=tags]').val(JSON.stringify(timeTags));
        
        var modal = $('#confirmationModal');
        $.ajax({
            url: form.attr('action'),
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken
            },
            type: 'POST',
            dataType: 'json',
            data: form.serializeArray(),
            success: function (data) {
                console.log(data);
                if(data['status'] === 0){
                    modal.find('div.modal-header').removeClass('bg-error').addClass('bg-success');
                } else {
                    modal.find('div.modal-header').removeClass('bg-success').addClass('bg-danger');
                }
                modal.find('#confirmationModalLabel').text(data['message']['title']);
                modal.find('div.modal-body').text(data['message']['text']);
                modal.modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error');
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            },
        });
        
    }
    
    function setTimer(time){
        var interval = setInterval(function() {
            time--;
            var min = Math.floor(time / 60);
            var sec = time % 60;
            var string = (min > 0? min + 'm ' : '') + sec + 's';
            $('#countdownCounter').text(string);
            if (time <= 0) {
                clearInterval(interval);
                location.reload();
            }
        }, 1000);
    }

</script>
@endsection