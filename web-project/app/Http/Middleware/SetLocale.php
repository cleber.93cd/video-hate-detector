<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use App\Http\Helpers\Helpers;

class SetLocale {

    const LOCALE_COOKIE_KEY = 'videoeval_locale';

    public function __construct(Application $app, Request $request) {
        $this->app = $app;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $defaultLocale = $request->cookie(self::LOCALE_COOKIE_KEY, config('app.locale'));
        if (array_key_exists($request->segment(1), config('app.available_locales'))) {
            $this->app->setLocale($request->segment(1));
        } elseif ($request->segment(1) === 'pt-br') {
            $this->app->setLocale('pt');
            return redirect(Helpers::get_current_url_with_locale('pt'));
        } else {
            $this->app->setLocale($defaultLocale);
        }

        $response = $next($request);

        if ($defaultLocale !== $this->app->getLocale()) {
            $response->cookie(self::LOCALE_COOKIE_KEY, $this->app->getLocale());
        }

        return $response;
    }

}
