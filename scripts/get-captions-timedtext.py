#!/usr/bin/python

# This script downloads the captions for the videos and save them in the 
# original format XML for each video and each language.

import datetime
import re
import os
import sys
import requests
import xml.etree.ElementTree as ET
from vhd_utils import *

# File to store the videos with no caption available
VIDEOS_NO_CAPTION = 'datasources/videos-no-caption-temp.json'
# Output folder in the laboratory computer
OUTPUT_FOLDER = '/media/cleber/Data/masters/captions/'
# Output folder in the personal laptop
#OUTPUT_FOLDER = '/media/cleber/34A3146A18A3F995/masters/captions/'
# Base URL to retrieve captions information
BASE_URL = 'https://www.youtube.com/api/timedtext'
# URL parameters for listing captions
LIST_URL = BASE_URL + '?v=%s&type=list'
#URL parameters for downloading captions
DOWNLOAD_URL = BASE_URL + '?v=%s&lang=%s'


def get_captions_list(video_id):
  '''
  Gets the captions available for a video from YouTube.
  '''
  captions = []
  try:
    response = requests.get(LIST_URL % video_id)
    root = ET.fromstring(response.content)
    for child in root:
      captions.append(child.attrib)
  except:
    print 'An error occurred while dowloading the captions list: %s' % (sys.exc_info()[0])

  print 'Captions list retrieved for video %s (%d caption(s))' % (video_id, len(captions))
  
  return captions

def download_captions(video_id, captions):
  '''
  Download the caption file for a video from YouTube.
  Args:
    video_id (str): Video to get the captions from.
    captions (list): Captions to be downloaded.

  Returns:
    bool: The result of the download. True if all captions were downloaded successfully, or False any failed.
  '''
  print 'Downloading captions for video %s...' % video_id
  try:
    for caption in captions:
      output_file = OUTPUT_FOLDER + video_id + '_' + caption['lang_code'] + '.xml'
      if os.path.isfile(output_file):
        print '  %s already downloaded.' % caption['lang_code']
      else:
        response = requests.get(DOWNLOAD_URL % (video_id, caption['lang_code']))
        with open(output_file, 'wb') as file:
          file.write(response.content)
        print '  %s downloaded.' % caption['lang_code']
    print 'Captions for video %s downloaded.' % video_id
    result = True
  except Exception as e:
    result = False
    print 'An error occurred while downloading the captions: %s' % (sys.exc_info()[0])
    print e
  
  return result


if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime

  if len(sys.argv) < 2:
    print 'ERROR: You must provide input file. The file should be an JSON list of videos details.'
    print '\nUsage:'
    print '  python get-captions-timedtext.py input.json\n'
  else:
    videos = load_json(sys.argv[1])
    missing_captions = list()
    missing_captions_counter = 0;
    
    if not os.path.exists(OUTPUT_FOLDER):
      os.makedirs(OUTPUT_FOLDER)
      print 'Captions folder created.'
    
    counter = 0
    total = len(videos)
    for video in videos:
      counter += 1
      print 'Getting captions list for video %s... (%d/%d)' % (video['id'], counter, total)
      captions = get_captions_list(video['id'])
      
      elegible_captions = filter(lambda caption: re.match(r'^\pt\"*', caption['lang_code']) or re.match(r'^\en\"*', caption['lang_code']), captions)
      if len(elegible_captions) == 0 or not download_captions(video['id'], elegible_captions):
        missing_captions.append(video['id'])
        missing_captions_counter += 1

    save_json_list(missing_captions, VIDEOS_NO_CAPTION)
    print 'Videos without captions listed in ' + VIDEOS_NO_CAPTION
    print 'Videos with captions: %d out of %d' % (len(videos)-missing_captions_counter, len(videos))
  
  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime)
