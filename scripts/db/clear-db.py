#!/usr/bin/python


'''
This script is responsible for managing database operations.
'''
import sys
import os
import mysql.connector
import json
import dateutil.parser
import datetime

sys.path.append(os.path.abspath('../'))

import utils as db_util
import vhd_utils as util


util.LOG_LEVEL = util.LOG_DEBUG


if __name__ == '__main__':
	startTime = datetime.datetime.now()
	util.log_info('Start time: ' + str(startTime))

	mydb = db_util.get_connector()
	cursor = mydb.cursor()

	tables = ['content_details', 'localized', 'statistics', 'thumbnails', 'yt_video_snippets']
	query = 'DELETE FROM %s'

	for table in tables:
		try:
			util.log_debug('SQL Query: ' + (query % table))
			cursor.execute(query % table)
			mydb.commit()
			util.log_debug(str(cursor.rowcount) + ' record(s) affected.')
		except Exception as e:
			util.log_error('Error for table ' + table + ': ' + str(e))

	endTime = datetime.datetime.now()
	util.log_info('End time: ' + str(endTime))
	util.log_info('Elapsed time: ' + str(endTime - startTime))
