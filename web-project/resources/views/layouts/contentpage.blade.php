@extends('layouts.app')

@section('htmlheader_title')
@yield('title')
@endsection

@section('in-page-title')
@yield('title')
@endsection

@section('main-content')
@include('partials.pagetitle')
@yield('content')
@endsection