@extends('layouts.contentpage')

@section('title')
{{ucwords(mb_strtolower(trans('general.videos')))}}
@endsection

@section('content')
<div class="container mtb">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <h4><i class='fa fa-tags'></i> {{ucwords(mb_strtolower(trans('general.videos')))}}</h4>
                    </div>
                </div>
            </div>
            <div class="hline"></div>
            <table class="table table-responsive table-striped" id="videosTable">
                <thead>
                    <tr>
                        <th>{{trans('general.my_annotations_video_id')}}</th>
                        <th>{{trans('general.video_title')}}</th>
                        <th>{{trans('general.duration')}}</th>
                        <th>{{trans('general.video_audio_language_label')}}
                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{trans('general.video_audio_language_label_tooltip')}}"></i>
                        </th>
                        <th>{{trans('general.video_downloaded')}}</th>
                        <th>{{trans('general.video_caption')}}
                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{trans('general.video_caption_tooltip')}}"></i>
                        </th>
                        <th>{{trans('general.video_ready')}}
                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{trans('general.video_ready_tooltip')}}"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class='row'>
        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
            <div class="alert alert-info" role="alert">
                <i class='fa fa-exclamation-triangle'></i><strong> {{trans('general.hidden_filters')}}</strong><br/>
                <p><ul class="list-group">
                    <li class="list-group-item">{{trans('general.duration') . ' = ' . json_encode(config('vhd.duration_filter'))}}</li>
                </ul></p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js"></script>

<script>
    window.onload = function () {
        setupDatables($("#videosTable"));
    };
    function setupDatables(tableElement) {
        tableElement.DataTable({
            searching: false,
            dom: 'frtlip',
            responsive: true,
            pageLength: 10,
            order: [[4, 'desc']],
            columns: [
                {
                    data: 'id',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        if (type === 'display' && row.downloaded === 1) {
                            return '<code><a href="' + location.href
                                    + '/' + row.id + '">' + data + '</a></code>';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'title',
                    render: function (data, type, row, meta) {
                        if (type === 'display' && row.downloaded === 1) {
                            return '<a href="' + location.href
                                    + '/' + row.id + '">' + data + '</a>';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'duration',
                    className: 'text-right',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            //ISO format and start: 1970-01-01T00:00:00.000Z
                            origin = new Date(null);
                            date = new Date(1000*data);
                            years = date.getUTCFullYear() - origin.getUTCFullYear();
                            months = date.getUTCMonth() - origin.getUTCMonth();
                            days = date.getUTCDate() - origin.getUTCDate();
                            /*hours = date.getUTCHours() - origin.getUTCHours();
                            minutes = date.getUTCMinutes() - origin.getUTCMinutes();
                            seconds = date.getUTCSeconds() - origin.getUTCSeconds();*/
                            var string = "";
                            if(years > 0){
                                string += years + 'y ';
                            }
                            if(months > 0){
                                string += months + 'm ';
                            }
                            if(days > 0){
                                string += days + 'd ';
                            }
                            
                            return string + date.toISOString().substr(11, 8);
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'default_audio_language',
                    className: 'text-center'
                },
                {
                    data: 'downloaded',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            var iconType;
                            var iconText;
                            if(data === 1){
                                iconType = 'success';
                                iconText = 'check-circle';
                            } else if (data === 0){
                                iconType = 'danger';
                                iconText = 'times-circle';
                            } else {
                                iconType = 'warning';
                                iconText = 'circle';
                            }
                            return '<span class="text-' + iconType
                                    + '"><i class="fa fa-' + iconText + '"></i></span>';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'caption',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            var iconType;
                            var iconText;
                            if(data === 1){
                                iconType = 'success';
                                iconText = 'check-circle';
                            } else if (data === 0){
                                iconType = 'danger';
                                iconText = 'times-circle';
                            } else {
                                iconType = 'warning';
                                iconText = 'circle';
                            }
                            return '<span class="text-' + iconType
                                    + '"><i class="fa fa-' + iconText + '"></i></span>';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'ready',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            var iconType;
                            var iconText;
                            if(data === 1){
                                iconType = 'success';
                                iconText = 'check-circle';
                            } else if (data === 0){
                                iconType = 'danger';
                                iconText = 'times-circle';
                            } else {
                                iconType = 'warning';
                                iconText = 'circle';
                            }
                            return '<span class="text-' + iconType 
                                    + '"><i class="fa fa-' + iconText + '"></i></span>';
                        } else {
                            return data;
                        }
                    }
                }
            ],
            serverSide: true,
            ajax: location.href,
        });
    }
</script>
@endsection