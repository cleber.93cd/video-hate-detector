#!/usr/bin/python

'''
This script contains helpful funcions to be used while handling the videos.
VHD stands for Video Hate Detector
'''
import json
import isodate
import re

def load_json(filename):
	'''
	Loads the JSON with a list.
	If an error occur, return an empty list.
	'''
	json_list = []
	log_info('Loading JSON from ' + filename)
	try:
		with open(filename, 'r') as handle:
			json_list = json.load(handle)
	except Exception as e:
	    log_error(e)
	log_info('%d items(s) loaded for file %s' %(len(json_list), filename))
	return json_list

def save_json_list(items, filename):
	'''
	Saves the items list to a file in the JSON format with one item per line.
	'''
	print 'Saving %d item(s) in %s...' % (len(items), filename)

	try:
		with open(filename, 'w') as outfile:
		    outfile.write('[')

		with open(filename, 'a') as outfile:
			items_list = map(lambda item: json.dumps(item), items)
			outfile.write(',\n'.join(items_list))
			outfile.write(']')
	except Exception as e:
  		print 'An error ocurred while saving the items: ' + str(e)

def remove_duplicates(filename):
	'''
	Removes the duplicates from the videos list file and overrides the file with the new list.
	'''
	print 'Removing duplicates from %s...' % filename
	videos = load_json(filename)
	total = len(videos)
	videos_set = set(videos)
	duplicates = total - len(videos_set)

	print 'Total videos: %d (%d duplicate(s) removed)' % (len(videos_set), duplicates)
	save_json_list(videos_set, filename)


def convert_duration(google_duration):
	'''
	Converts the duration of a video from google format to seconds.
	'''
	duration = isodate.parse_duration(google_duration)
	return duration.total_seconds()


def filter_defaultAudioLanguage_PT_EN(videos):
  '''
  Filters a list of videos, returning only the ones which have the defaultAudioLanguage attribute set as 'pt' or 'en'
  '''
  AUDIO = ['pt', 'en']
  print 'Filtering videos with default audio language as ', AUDIO
  filtered_videos = []
  for video in videos:
    default_audio_language = video['snippet'].get('defaultAudioLanguage', '')
    if re.match(r'^(pt|en)', default_audio_language):
      filtered_videos.append(video)

  print 'Prior to filter: %d' % len(videos)
  print 'After filter: %d (%d videos filtered)' % (len(filtered_videos), len(videos) - len(filtered_videos))

  return filtered_videos


LOG_DEBUG = 0
LOG_INFO = 1
LOG_WARNING = 2
LOG_ERROR = 3

LOG_LEVEL = LOG_DEBUG
def log(level, msg):
	'''
	Logs a message to the default output.
	'''
	if level+1 > LOG_LEVEL:
		print msg

def log_debug(msg):
	log(LOG_DEBUG, '[DEBUG] ' + msg)

def log_info(msg):
	log(LOG_INFO, '[INFO] ' + msg)

def log_warning(msg):
	log(LOG_WARNING, '[WARNING] ' + msg)

def log_error(msg):
	log(LOG_ERROR, '[ERROR] ' + msg)
