#!/usr/bin/python

'''
This script summarizes size of the videos in the file system according to a filter applied in the database. The size is shown per file type and in MB.
'''

from __future__ import unicode_literals
import datetime
import youtube_dl
import json
import os
import sys
import vhd_utils as util
import isodate
import collections
import glob

util.LOG_LEVEL = util.LOG_INFO

##### MAIN #####

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime

  if len(sys.argv) < 3:
    print 'ERROR: You must provide an input JSON file with an attribute id for each video to be checked, and the path to the videos folder.'
    print '\nUsage:'
    print '  python summarize-size.py input.json folder\n'
  else:
    input_file = sys.argv[1]
    input_folder = sys.argv[2]
    
    videos = util.load_json(sys.argv[1])
    sizes = {}
    util.log_info('Processing %d videos' % len(videos))
    for video in videos:
      files = glob.glob('%s/%s*' % (input_folder, video['id']))
      util.log_info('Video %s: %d files found' % (video['id'], len(files)))
      for filename in files:
        if(filename[-4:] not in sizes):
          sizes[filename[-4:]] = 0
        sizes[filename[-4:]] += os.path.getsize(filename)
      util.log_debug('Sizes so far: ' + str(sizes))
    
    util.log_info('Sizes:')
    for size in sizes:
      util.log_info(size + ': ' + str(float(sizes[size])/(1024*1024)) + 'MB')

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime) 

