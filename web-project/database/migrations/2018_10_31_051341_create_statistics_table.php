<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
            $table->char('yt_video_snippets_id', 11);
            $table->foreign('yt_video_snippets_id')
                    ->references('id')
                    ->on('yt_video_snippets')
                    ->onDelete('cascade');
            $table->primary('yt_video_snippets_id');
            $table->mediumText('view_count')->nullable();
            $table->mediumText('like_count')->nullable();
            $table->mediumText('dislike_count')->nullable();
            $table->mediumText('favorite_count')->nullable();
            $table->mediumText('comment_count')->nullable();
            $table->dateTime('last_checked');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
