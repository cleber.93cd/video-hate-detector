#!/usr/bin/python

'''
This script downloads a video from YouTube
'''

from __future__ import unicode_literals
import datetime
import youtube_dl
import json
import os
import sys
from vhd_utils import *

##### CONSTANTS #####
YOUTUBE_URL = 'https://www.youtube.com/'

#### GLOBAL VARS #####
output_folder = ''

##### METHODS AND CLASSES #####

class MyLogger(object):
  def debug(self, msg):
    pass

  def warning(self, msg):
    pass

  def error(self, msg):
    print(msg)


def process_progress(d):
  '''
  Processes the download progress, showing messages
  '''
  filename = d['filename'][len(output_folder):]
  key = filename[:filename.rfind('.')]

  if key in download_statuses and download_statuses[key]['downloaded'] == None:
    download_statuses[key]['downloaded'] = False
    print 'Downloading ' + key

  if d['status'] == 'downloading':
    pass
  elif d['status'] == 'finished':
    if key in download_statuses:
      download_statuses[key]['downloaded'] = True
    print 'Video %s downloaded. Now converting...' % key
  elif d['status'] == 'error':
    print 'There was an error during the download!'


def get_status_dict(videos_list):
  '''
  Gets a dictionary with information about the download status of the videos.
  The key of the dictionary is the video key.
  The value is an object with the following properties: url, downloaded
  '''
  return {
    video_id: {
    'url': YOUTUBE_URL + 'watch?v=' + video_id,
    'downloaded': None
    } for video_id in videos_list}


def print_statistics():
  '''
  Prints the statistics of the downloads
  '''
  downloaded = []
  notDownloaded = []
  notStarted = []
  for key, value in download_statuses.iteritems():
    if value['downloaded'] == True:
      downloaded.append(key)
    elif value['downloaded'] == False:
      notDownloaded.append(key)
    elif value['downloaded'] == None:
      notStarted.append(key)
  total = len(download_statuses)
  print 'Downloaded: %d/%d' % (len(downloaded), total)
  print 'Not Downloaded: %d/%d' % (len(notDownloaded), total) + ': ' + str(notDownloaded)
  print 'Not Started: %d/%d' % (len(notStarted), total) + ': ' + str(notStarted)

def filter_videos_ids(video):
  return video['id']

##### MAIN #####

download_statuses = {}

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime

  if len(sys.argv) < 3:
    print 'ERROR: You must provide an input file and a destination folder. The file should be an JSON list of videos details.'
    print '\nUsage:'
    print '  python get-captions-timedtext.py input.json output_folder\n'
  else:
    output_folder = sys.argv[2] + '/'
    ydl_opts_video = {
      'outtmpl': output_folder + '%(id)s.%(ext)s',
      'format': 'worst',
      'logger': MyLogger(),
      'progress_hooks': [process_progress],
    }
    ydl_opts_audio = {
      'outtmpl': output_folder + '%(id)s.%(ext)s',
      'keepvideo': False,
      'format': 'bestaudio',
      'postprocessors': [{
          'key': 'FFmpegExtractAudio',
          'preferredcodec': 'mp3'
      }],
      'logger': MyLogger(),
      'progress_hooks': [process_progress],
    }

    videos = load_json(sys.argv[1])
    videos_list = map(filter_videos_ids, videos)
    
    download_statuses = get_status_dict(videos_list)
    total = len(download_statuses)
    index = 1
    for key in download_statuses:
      print 'Video %d out of %d (%.2f%%)' % (index, total, float(index*100)/total)
      index += 1
      try:
        if(os.path.isfile(output_folder + key + '.mp3')):
          print 'The video ' + key + ' was processed already.'
          download_statuses[key]['downloaded'] = True
          continue
        with youtube_dl.YoutubeDL(ydl_opts_video) as ydl:
          ydl.download([download_statuses[key]['url']])
        with youtube_dl.YoutubeDL(ydl_opts_audio) as ydl:
          ydl.download([download_statuses[key]['url']])
      except Exception as e:
        print 'Error for video %s: %s' % (key, sys.exc_info()[0])
        try:
          print e
        except Exception as e:
          print 'Error while printing error message: %s' % sys.exc_info()[0]
    
    print_statistics()

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime) 

