<div id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <h4>
                    {{ucwords(strtolower(trans('general.about')))}}
                    <div class="pull-right h5">
                        v.{{config('app.version')}}
                    </div>
                </h4>
                <div class="hline-w"></div>
                <p>{!!trans('general.about_desc')!!}</p>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <h4>{{trans('general.support')}}</h4>
                <div class="hline-w"></div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="http://ufrgs.br" target="_blank">
                            <img class="img-rounded" src="{{asset('img/logo_ufrgs.jpg')}}">
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="http://inf.ufrgs.br" target="_blank">
                            <img class="img-rounded" src="{{asset('img/logo_inf.jpg')}}">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6">
                <h4>{{trans('general.sponsorship')}}</h4>
                <div class="hline-w"></div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a href="http://cnpq.br" target="_blank">
                            <img class="img-rounded" src="{{asset('img/logo_cnpq.jpg')}}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12">
                <h4>{{trans('general.our_address')}}</h4>
                <div class="hline-w"></div>
                <p>{!!trans('general.our_address_desc')!!}</p>
            </div>
        </div>
    </div>
</div>