<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Redirect;
use App\User;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    /* public function redirectPath()
      {
      if (method_exists($this, 'redirectTo')) {
      return $this->redirectTo();
      }

      return Helpers::url(property_exists($this, 'redirectTo') ? $this->redirectTo : '/home');
      } */

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request) {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user()) ?: redirect()->intended(Helpers::url($this->redirectPath()));
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect(Helpers::url('/'));
    }
    
    public function successfullSocialLogin(User $user){
        $localUser = User::where('email', $user->email)->first();
        if ($localUser === NULL) {
            $user->save();
            $localUser = $user;
            Log::info('Social user ' . $user->email . ' has been registered.');
        } else {
            Log::debug('Social user ' . $user->email . ' is already registered.');
        }
        Auth::login($localUser);
        $redirectUrl = session('url.intended', Helpers::url($this->redirectPath()));
        Log::info('User ' . $user->email . ' signed in. Redirecting to: ' . $redirectUrl);
        return redirect($redirectUrl);
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToGoogle() {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleGoogleResponse() {
        $googleUser = Socialite::driver('google')->user();
        $user = new User([
                'name' => $googleUser->name,
                'email' => $googleUser->email,
                'password' => bcrypt($googleUser->token)
        ]);
        return $this->successfullSocialLogin($user);
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebook() {
        $config = config('services.facebook');
        $uri = $config['auth_uri'];
        $params['redirect'] = Helpers::url($config['redirect']);
        $params['language'] = app()->getLocale();
        return Redirect::away($uri . '?' . http_build_query($params));
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleFacebookResponse() {
        if(!request()->has('access_token') || request()->access_token === 'false'){
            return redirect(Helpers::url('login'))
                    ->withErrors([
                'social' => trans('auth.failed_social_attempt')
            ]);
        }
        $accessToken = request()->access_token;
        $userId = request()->user_id;
        try {
            $isValid = $this->validateFacebookAuthentication($accessToken, $userId);
        } catch (\Exception $exception) {
            $isValid = FALSE;
            Log::error($exception->getMessage());
        }
        if ($isValid) {
            Log::debug('The Facebook authentication worked.');

            $facebookUser = $this->getFacebookData($userId, $accessToken);
            $user = new \App\User([
                'name' => $facebookUser->name,
                'email' => $facebookUser->email,
                'password' => bcrypt($accessToken)
            ]);
            return $this->successfullSocialLogin($user);
        } else {
            Log::error('The Facebook authentication did not work.');
            return redirect(Helpers::url('login'))
                    ->withErrors([
                'social' => trans('auth.failed_social_attempt')
            ]);
        }
    }

    public function validateFacebookAuthentication($userToken, $userId) {
        $config = config('services.facebook');
        
        Log::debug('Validating user ID ' . $userId . ' and access token ' . $userToken);

        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://graph.facebook.com/oauth/access_token', [
            'query' => [
                'client_id' => $config['client_id'],
                'client_secret' => $config['client_secret'],
                'redirect_uri' => $config['redirect'],
                'grant_type' => 'client_credentials'
            ]
        ]);
        $appToken = json_decode($response->getBody())->access_token;

        $responseValidation = $client->get('https://graph.facebook.com/debug_token', [
            'query' => [
                'input_token' => $userToken,
                'access_token' => $appToken
            ]
        ]);
        $validationData = json_decode($responseValidation->getBody());

        $isValid = $validationData->data->is_valid
                && $validationData->data->app_id === $config['client_id']
                && $validationData->data->user_id === $userId;

        return $isValid;
    }

    public function getFacebookData($userId, $accessToken) {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://graph.facebook.com/' . $userId, [
                'query' => [
                    'access_token' => $accessToken,
                    'fields' => 'email,name,id'
                ]
            ]);
            Log::debug('Facebook data body (status code = '
                    . $response->getStatusCode(). '):' . $response->getBody());
            $userData = json_decode($response->getBody());
            if(!isset($userData->email) || $userData->email === ''){
                $userData->email = $userId . '@facebook.com';
                Log::warning('Facebook user with ID ' . $userId
                        . ' does not have an email. Setting it as '
                        . $userData->email);
            }
            return $userData;
        } catch (\Exception $exception) {
            Log::error('Error when getting user facebook data');
            Log::error($exception->getMessage());
            return NULL;
        }
    }

}
