<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    
    'login' => 'Login',
    'log_in' => 'Log in',
    'remember_me' => 'Remember Me',
    'forgot_password' => 'Forgot Your Password?',
    'register' => 'Register',
    'register_yourself' => 'Register yourself',
    'login_alternative' => 'OR LOG IN USING',
    'failed_social_attempt' => 'Your attempt failed. Try again or use a different method.',
    
    'name_field' => 'Name',
    'email_field' => 'E-Mail address',
    'password_field' => 'Password',
    'password_confirmation_field' => 'Confirm Password',
    'recaptcha_unchecked' => 'You must check the recaptcha field',

];
