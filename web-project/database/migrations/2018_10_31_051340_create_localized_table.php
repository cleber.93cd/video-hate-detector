<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalizedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localized', function (Blueprint $table) {
            $table->char('yt_video_snippets_id', 11);
            $table->foreign('yt_video_snippets_id')
                    ->references('id')
                    ->on('yt_video_snippets')
                    ->onDelete('cascade');
            $table->primary('yt_video_snippets_id');
            $table->string('title', 1000);
            $table->text('description', 10000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localized');
    }
}
