<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThumbnailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thumbnails', function (Blueprint $table) {
            $table->char('yt_video_snippets_id', 11);
            $table->foreign('yt_video_snippets_id')
                    ->references('id')
                    ->on('yt_video_snippets')
                    ->onDelete('cascade');
            $table->string('thumbnail_key', 8);
            $table->primary(['yt_video_snippets_id', 'thumbnail_key']);
            $table->string('url', 2083);
            $table->integer('width');
            $table->integer('height');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thumbnails');
    }
}
