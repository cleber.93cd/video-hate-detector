# Pipeline overview
The steps below describe the order the scripts should usually be run:
1. search-videos.py
1. merge-search-result.py
1. get-details.py
1. filter-hasDefaultAudioLanguage.py
1. get-captions-timedtext.py
1. split-set.py (optional)
1. download-videos.py
