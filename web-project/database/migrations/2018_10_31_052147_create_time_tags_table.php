<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('annotation_id')->unsigned();
            $table->foreign('annotation_id')
                    ->references('id')
                    ->on('annotations')
                    ->onDelete('cascade');
            $table->integer('moment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_tags');
    }
}
