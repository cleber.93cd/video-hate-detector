<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentDetail extends Model {
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function video() {
        return $this->belongsTo('App\YTVideoSnippet');
    }

}
