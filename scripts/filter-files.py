#!/usr/bin/python

'''
This script creates a folder and moves the videos files filtered in the database to a that folder so they can be sent to a server later.
'''

import datetime
import os
import sys
import vhd_utils as util
import glob
import shutil

util.LOG_LEVEL = util.LOG_INFO

##### MAIN #####

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime

  if len(sys.argv) < 4:
    print 'ERROR: You must provide an input JSON file with an attribute id for each video to be checked, and the path to the folder where the videos and the name of the new folder.'
    print '\nUsage:'
    print '  python summarize-size.py input.json source_folder new_folder\n'
  else:
    input_file = sys.argv[1]
    input_folder = sys.argv[2]
    new_folder = sys.argv[3]
    if not os.path.exists(new_folder):
      os.makedirs(new_folder)
    
    videos = util.load_json(sys.argv[1])
    util.log_info('Processing %d videos' % len(videos))
    counter = 1
    total = len(videos)
    for video in videos:
      files = glob.glob('%s/%s*' % (input_folder, video['id']))
      util.log_info('[%d out of %d] Moving %d files for video %s' % (counter, total, len(files), video['id']))
      counter += 1
      for filename in files:
        util.log_debug('Moving %s to %s' % (filename, new_folder))
        shutil.move(filename, new_folder)

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime) 

