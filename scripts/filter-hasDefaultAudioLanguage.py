#!/usr/bin/python

import datetime
import re
import sys
from vhd_utils import *

AUDIO = ['pt', 'en']

if __name__ == '__main__':
  startTime = datetime.datetime.now()
  print 'Start time: ', startTime
  if len(sys.argv) != 3:
    print 'ERROR: You must provide 1 input file and 1 output file in order to perform the filter. The files should be an JSON list of videos details.'
    print '\nUsage:'
    print '  python filter-hasDefaultAudioLanguage.py input.json output.json\n'
  else: 
    print 'Loading videos...'
    videos = load_json(sys.argv[1])

    filtered_videos = vhd_utils.filter_defaultAudioLanguage_PT_EN(videos)
    save_json_list(filtered_videos, sys.argv[2])

  endTime = datetime.datetime.now()
  print 'End time: ', str(endTime)
  print 'Elapsed time: ', str(endTime - startTime)
