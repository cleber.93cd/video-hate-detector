@extends('layouts.app')

@section('htmlheader_title')
{{ucwords(mb_strtolower(trans('general.my_annotations')))}}
@endsection

@section('in-page-title')
{{ucwords(mb_strtolower(trans('general.my_annotations')))}}
<div class="btn-group btn-group-xs pull-right" role="group" aria-label="..." style="margin-right: 15px">
    @foreach(config("app.available_locales") as $localeKey => $localeValue)
    <button type="button" class="btn btn-default {{ 'pt' === $localeKey ? 'active' : 'disabled' }}">
        <img src="{{asset('img/flags/blank.gif')}}" class="flag flag-{{$localeValue['flag_id']}}" alt="{{$localeValue['flag_name']}}" />
        <span class="hidden-xs">{{$localeValue['language']}}</span>
        <span class="hidden-lg hidden-md hidden-sm">{{strtoupper($localeKey)}}</span>
    </button>
    @endforeach
</div>
@if($userName !== '')
<div class="h4">{{$userName}}</div>
@endif
@endsection

@section('main-content')
@include('partials.pagetitle')
<div class="container mtb">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <h4><i class='fa fa-tasks'></i> {{trans('general.my_progress') }}</h4>
            <div class="hline"></div><br/>
            <div class="progress" style="height: 40px">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="min-width: 4em; width: {{$myAnnotationsPT*100/($myAnnotationsPT + $possibleHelp)}}%">
                    <b>{{$myAnnotationsPT}}</b><br/>
                    {{number_format($myAnnotationsPT*100/($myAnnotationsPT + $possibleHelp), 2)}}%
                </div>
            </div>
            <canvas id="annotationsChart" height="250" class='hidden-sm'></canvas>
            <canvas id="annotationsChartSM" class='hidden-lg hidden-md hidden-xs'></canvas>
            <br/>
            <div class="alert alert-info" role="alert">
                @if($possibleHelp === 0)
                <i class='fa fa-check-circle'></i> <strong>{{trans('general.thank_you')}}</strong><br/>
                {{trans_choice('general.amount_help', $possibleHelp)}}
                <div class="text-center">
                    <i class='fa fa-star fa-5x'></i>
                </div>
                @else
                <i class='fa fa-info-circle'></i> <strong>{{trans('general.help_reach_goal')}}</strong><br/>
                {{trans_choice('general.amount_help', $possibleHelp) . " $possibleHelp " . trans_choice('general.video', $possibleHelp)}}.<br/>
                <a class='btn btn-info btn-block' href='{{Helpers::url('annotations/new')}}'>{{trans('general.start_helping')}}</a>
                @endif
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <h4><i class='fa fa-tags'></i> {{ucwords(mb_strtolower(trans('general.my_annotations')))}}</h4>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-theme no-border" href="{{Helpers::url('annotations/new')}}" data-toggle="tooltip" data-placement="bottom" title="{{trans('general.annotate_new')}}">
                            <i class="fa fa-plus-circle"></i>
                            <span class='hidden-xs'> {{trans('general.annotate_new')}}</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="hline"></div>
            <table class="table table-responsive" id="annotationsTable">
                <thead>
                    <tr>
                        <th>{{trans('general.my_annotations_time')}}</th>
                        <th>{{trans('general.my_annotations_video_id')}}</th>
                        <th>{{trans('general.my_annotations_label')}}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="viewVideoModal" tabindex="-1" role="dialog" aria-labelledby="viewVideoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-top-left-radius: 6px;border-top-right-radius: 6px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="viewVideoModalLabel">
                    <i class='fa fa-film'></i> {{ucwords(trans_choice('general.video',1))}}:
                    <a target="_blank" href="">
                        <span></span>
                        <i class="fa fa-external-link"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body" style='padding: 0px;'>
                <video class="img-responsive center-block" controls style="width: 100%;">
                    <p>Your browser doesn't support HTML5 video.</p>
                </video>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js"></script>
<script src="{{asset('js/Chart.bundle.min.js')}}"></script>
<script>
    var locale = "{!!app()->getLocale()!!}";
    var isAdmin = {!!(Auth::check() && Auth::user()->is_admin) ? 'true': 'false'!!};
    var annotationsVideosLabel = "{!!trans('general.annotation_performed')!!}";
    var beforeEndLabel = "{!!trans('general.before_end')!!}";
    var labelOffensive = {!!json_encode(trans('general.label_offensive'))!!};
    var labelNotOffensive = {!!json_encode(trans('general.label_not_offensive'))!!};
    var annotations;
    var annotationsChart;
    var annotationsChartSM;
    window.onload = function () {
        annotations = {!! json_encode($annotations) !!};
        setupDatables($("#annotationsTable"), annotations);
        setupAnnotationsChart(annotations);
    };

    $('#viewVideoModal').on('show.bs.modal', function (event) {
        var id = event.relatedTarget.innerText;

        $('#viewVideoModalLabel>a').attr("href", "https://youtube.com/watch?v=" + id);
        $('#viewVideoModalLabel>a>span').text(id);

        var metadata = [];
        for (var index = 0; index < annotations.length; index++) {
            if (annotations[index].video_id === id) {
                metadata = annotations[index].videoMetadata;
                break;
            }
        }
        var sources = "";
        metadata.forEach(function (element) {
            sources += '<source src="' + element.url + '" type="' + element.type + '">';
        });

        $(event.target).find('video').prepend(sources)
    });
    $('#viewVideoModal').on('hidden.bs.modal', function (event) {
        $('#viewVideoModalLabel>a').attr("href", "");
        $('#viewVideoModalLabel>a>span').text("");
        $(event.target).find('video > source').remove();
        $(event.target).find('video')[0].load();
    });
    function setupDatables(tableElement, data) {
        data.forEach(function(element, index){
            data[index].previous = (index === 0 ? null: data[index-1]);
        });
        tableElement.DataTable({
            data: data,
            searching: false,
            dom: 'frtip',
            responsive: true,
            pageLength: 10,
            order: [[0, 'desc']],
            columns: [
                {
                    data: 'created_at',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        var date = new Date(row.created_at);
                        if (type === 'display') {
                            if(locale === 'en'){
                            niceDate = ('0' + (date.getMonth() + 1)).slice(-2) + '/'
                                    + ('0' + date.getDate()).slice(-2) + '/'
                                    + date.getFullYear();
                            } else {
                                niceDate = ('0' + date.getDate()).slice(-2) + '/'
                                    + ('0' + (date.getMonth() + 1)).slice(-2) + '/'
                                    + date.getFullYear();
                            }
                            niceHour = ('0' + date.getHours()).slice(-2)
                                    + ':' + ('0' + date.getMinutes()).slice(-2)
                                    + ':' + ('0' + date.getSeconds()).slice(-2);
                            var obs = '';
                            if(isAdmin && row.previous !== null){
                                var started_at = new Date(row.created_at);
                                started_at.setSeconds(started_at.getSeconds() - row.duration);
                                var previous_ended_at = new Date(row.previous.created_at);
                                if(previous_ended_at > started_at){
                                    obs = ' <span class="label label-danger" data-toggle="tooltip" data-placement="bottom" title="'
                                            + beforeEndLabel +'">' + (previous_ended_at.getTime() - started_at.getTime())/1000 + '</span>';
                                }
                            }
                            return niceDate + ' ' + niceHour + obs;
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'video_id',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            return '<code><button class="btn btn-link btn-open-video" style="padding:0px;border:0px;" data-toggle="modal" data-target="#viewVideoModal">'
                                    + data + '</button></code>';
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'time_tags_count',
                    className: 'text-center',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            var labelType;
                            var labelText;
                            if (data === 0) {
                                labelType = 'success';
                                labelText = labelNotOffensive;
                            } else {
                                labelType = 'danger';
                                labelText = labelOffensive;
                            }
                            return '<span class="label label-' + labelType + '">'
                                    + labelText + '</span>';
                        } else {
                            return data;
                        }
                    }
                },
            ],
        });
    }
    
    function setupAnnotationsChart(annotations){
        var chartData = {};
        var parts, date;
        annotations.forEach(function(annotation){
            parts = annotation.created_at.split(' ');
            parts = parts[0].split('-');
            if (locale === 'en') {
                date = parts[1] + '/' + parts[2] + '/' + parts[0];
            } else {
                date = parts[2] + '/' + parts[1] + '/' + parts[0];
            }
            if(typeof chartData[date] === 'undefined'){
                chartData[date] = {
                    annotation_date: date,
                    counter: 0,
                };
            }
            chartData[date].counter++;
        });
        chartData = Object.keys(chartData).map(date => chartData[date]);
        var chartOptions = {
            type: 'line',
            data: {
                labels: chartData.map(element => element.annotation_date),
                datasets: [
                    {
                        data: chartData.map(element => element.counter),
                        borderColor: '#00b3fe',
                        label: annotationsVideosLabel,
                        fill: false,
                        lineTension: 0,
                        pointHitRadius: 10,
                    }
                ]
            },
            options: {
                legend: {
                    display: false,
                },
            }
        };
        annotationsChart = new Chart($('#annotationsChart'), chartOptions);
        annotationsChartSM = new Chart($('#annotationsChartSM'), chartOptions);
    }
</script>
@endsection