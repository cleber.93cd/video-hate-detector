<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('login/google', 'Auth\LoginController@redirectToGoogle');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleResponse');
Route::get('login/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookResponse');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/annotations/ranking', 'AnnotationController@getRankingPage');
    
    Route::get('/annotations', 'AnnotationController@getAll');
    Route::get('/annotations/new', 'AnnotationController@getAnnotationForm');
    Route::post('/annotations/new', 'AnnotationController@create');
    
    Route::get('/videos', 'VideoController@getAll');
    Route::get('/videos/{id}', 'VideoController@viewVideo');
    Route::post('/videos/{id}', 'VideoController@updateVideo');
    
    Route::get('/statistics', 'StatisticController@index');
    Route::get('/users/{userId}/annotations', 'AnnotationController@getAll')
            ->where('userId', '[0-9]+')
            ->name('user_annotations');
});