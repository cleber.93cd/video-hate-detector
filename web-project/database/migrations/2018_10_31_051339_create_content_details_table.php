<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_details', function (Blueprint $table) {
            $table->char('yt_video_snippets_id', 11);
            $table->foreign('yt_video_snippets_id')
                    ->references('id')
                    ->on('yt_video_snippets')
                    ->onDelete('cascade');
            $table->primary('yt_video_snippets_id');
            $table->integer('duration');
            $table->string('dimension', 2);
            $table->string('definition', 2);
            $table->boolean('caption');
            $table->boolean('licensed_content');
            $table->string('region_restricted', 1000)->nullable();
            $table->string('projection', 11);
            $table->boolean('has_custom_thumbnail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_details');
    }
}
