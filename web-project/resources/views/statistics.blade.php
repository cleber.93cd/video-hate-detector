@extends('layouts.contentpage')

@section('title')
{{ucwords(mb_strtolower(trans('general.statistics')))}}
@endsection

@section('content')
<div class="container mtb">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>{{trans('general.overview')}}</h3>
            <div class="hline"></div>
            <h4>{{trans('general.language_distribution') . ': ' . $total}}</h4>
            <div class="progress" style="height: 40px">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="width: {{$pt['total']*100/$total}}%">
                    <b>PT: {{$pt['total'] . ' '. trans_choice('general.video', $pt['total'])}}</b><br/>
                    {{number_format($pt['total']*100/$total, 2)}}%
                </div>
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="width: {{$en['total']*100/$total}}%">
                    <b>EN: {{$en['total'] . ' '. trans_choice('general.video', $en['total'])}}</b><br/>
                    {{number_format($en['total']*100/$total, 2)}}%
                </div>
            </div>
            <h4>{{trans('general.language_distribution') . ' - ' . trans('general.filtered') . ': '. $totalFiltered}}</h4>
            <b>{{trans('general.filters')}}</b>: {{trans('general.duration') . ' = ' . json_encode(config('vhd.duration_filter'))}}
            <div class="progress" style="height: 40px">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="width: {{$pt['totalFiltered']*100/$totalFiltered}}%">
                    <b>PT: {{$pt['totalFiltered'] . ' '. trans_choice('general.video', $pt['totalFiltered'])}}</b><br/>
                    {{number_format($pt['totalFiltered']*100/$totalFiltered, 2)}}%
                </div>
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="width: {{$en['totalFiltered']*100/$totalFiltered}}%">
                    <b>EN: {{$en['totalFiltered'] . ' '. trans_choice('general.video', $en['totalFiltered'])}}</b><br/>
                    {{number_format($en['totalFiltered']*100/$totalFiltered, 2)}}%
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach(['pt', 'en'] as $language)
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h3>{{trans('general.statistics_' . $language)}}</h3>
            <div class="hline"></div>
            <h4>{{trans('general.status') . ': ' . ${$language}['totalFiltered']}}</h4>
            <div class="progress" style="height: 40px">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="min-width: 4em; width: {{${$language}['totalReady']*100/${$language}['total']}}%">
                    <b>{{${$language}['totalReady']}}</b><br/>
                    {{number_format(${$language}['totalReady']*100/${$language}['totalFiltered'], 2)}}%
                </div>
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="min-width: 4em; width: {{${$language}['totalRefused']*100/${$language}['totalFiltered']}}%">
                    <b>{{${$language}['totalRefused']}}</b><br/>
                    {{number_format(${$language}['totalRefused']*100/${$language}['totalFiltered'], 2)}}%
                </div>
            </div>
            <span class="label label-success">{{trans('general.ready')}}</span>
            <span class="label label-danger">{{trans('general.not_ready')}}</span>
            <h4>{{trans('general.annotations_count') . ': ' . ${$language}['totalVideos'] . ' (' . number_format(${$language}['totalVideos']*100/${$language}['totalReady'], 1) . '%)'}}</h4>
            <div class="progress" style="height: 40px">
                @foreach(array_reverse(${$language}['annotationCounters'], TRUE) as $key => $counter)
                @if($counter > 0)
                <div class="progress-bar progress-bar-{{(intval($key) === 3)?'success':((intval($key) === 2)?'info':((intval($key) === 1)?'warning':'danger'))}}"
                     role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     style="width: {{$counter*100/${$language}['totalReady']}}%"
                     data-toggle="tooltip" title="{{$counter . ' (' . number_format($counter*100/${$language}['totalReady'], 1) . '%)'}}">
                    <b>{{$counter}}</b><br/>
                    {{number_format($counter*100/${$language}['totalReady'], 1)}}%
                </div>
                @endif
                @endforeach
            </div>
            @foreach(array_reverse(${$language}['annotationCounters'], TRUE) as $key => $counter)
            <span class="label label-{{(intval($key) === 3)?'success':((intval($key) === 2)?'info':((intval($key) === 1)?'warning':'danger'))}}">
                {{$key . ' ' . trans_choice('general.annotation_name', $key)}}</span> x {{$counter}}
                @if(!$loop->last)
                +
                @endif
            @endforeach
            = <b>{{${$language}['totalAnnotations'] . ' ' . trans_choice('general.annotation_name', ${$language}['totalAnnotations'])}}</b>
            <h4>{{trans('general.agreement') . ' - ' . trans('general.last_evaluation')}}</h4>
            <span class="label label-success">{{trans('general.label_offensive')}}</span>
            <span class="label label-default">{{trans('general.label_not_offensive')}}</span>
            <div class='row'>
                @foreach(${$language}['agreement'] as $key =>$counter)
                <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                    <h5 class='h5'>{{trans('general.' . $key) . ': ' . $counter}}</h5>
                    <div class="progress" style="height: 40px">
                        @if(${$language}['agreementCounter'] > 0)
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                             style="width: {{$counter*100/${$language}['agreementCounter']}}%">
                            <b>{{$counter}}</b><br/>
                            {{number_format($counter*100/${$language}['agreementCounter'], 2)}}%
                        </div>
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                             style="width: {{(${$language}['agreementCounter']-$counter)*100/${$language}['agreementCounter']}}%; background-color: #999">
                            <b>{{${$language}['agreementCounter']-$counter}}</b><br/>
                            {{number_format((${$language}['agreementCounter']-$counter)*100/${$language}['agreementCounter'], 2)}}%
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

@section('page-scripts')

@endsection