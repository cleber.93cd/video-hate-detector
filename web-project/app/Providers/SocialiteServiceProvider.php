<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Laravel\Socialite\SocialiteServiceProvider as ServiceProvider;
use Laravel\Socialite\Contracts\Factory;
use App\Http\Helpers\Helpers;

class SocialiteServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton(Factory::class, function ($app) {
            return new CustomSocialiteManager($app);
        });
    }

}

class CustomSocialiteManager extends \Laravel\Socialite\SocialiteManager {

    /**
     * Format the callback URL, resolving a relative URI if needed.
     *
     * @param  array  $config
     * @return string
     */
    protected function formatRedirectUrl(array $config) {
        $redirect = value($config['redirect']);

        return Str::startsWith($redirect, '/') ? Helpers::url($redirect) : $redirect;
    }

}
