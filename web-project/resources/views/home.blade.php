@extends('layouts.app')

@section('htmlheader_title')
{{ucwords(mb_strtolower(trans('general.home')))}}
@endsection

@section('main-content')
<div id="headerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-6">
                <div class="row">
                    <div class="col-lg-12">
                        <h3><b>{{mb_strtoupper(trans('general.goal') . ': ' . $readyVideos) }}</b></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <canvas id="progressChart" height="{{$fullyAnnotatedVideos === $readyVideos ? '368px': (Auth::guest()? '343px':'410px')}}"></canvas>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if($fullyAnnotatedVideos === $readyVideos)
                        <div class="panel panel-default">
                            <div class="panel-body" style="padding-bottom: 0px;padding-top: 0px;">
                                <h4><i class="fa fa-star"></i> {{trans('general.goal_reached')}}</h4>
                                <p>{{trans('general.thank_you')}}</p>
                            </div>
                        </div>
                        @else
                        @if(Auth::guest())
                        <div class="panel panel-default">
                            <div class="panel-body" style="padding-bottom: 0px;padding-top: 0px;">
                                <h4><i class="fa fa-exclamation-triangle"></i> {{trans('general.help_reach_goal')}}</h4>
                                <p>
                                    {!!trans('general.auth_to_help', [
                                    'login_url' => route('login'),
                                    'login_string' => mb_strtolower(trans('auth.log_in')),
                                    'register_url' => route('register'),
                                    'register_string' => mb_strtolower(trans('auth.register_yourself')),
                                    ])!!}
                                </p>
                            </div>
                        </div>
                        @else
                        <a class='btn btn-theme btn-block' style="margin: 0px; margin-bottom: 20px"
                           href="{{Helpers::url('annotations')}}">{{ucwords(mb_strtolower(trans('general.my_annotations')))}}</a>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <h4><i class='fa fa-trophy'></i> {{trans('general.top_annotators')}}</h4>
                                </div>
                                @if(Auth::check())
                                <div class="pull-right">
                                    <a class="btn btn-theme no-border" href="{{Helpers::url('annotations/ranking')}}" data-toggle="tooltip" data-placement="bottom" title="{{trans('general.all')}}">
                                        <i class="fa fa-list-ol"></i>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="hline"></div>
                        @if(count($topAnnotators) === 0)
                        <p></p>
                        <div class="alert alert-info" role="alert">
                            <i class='fa fa-info-circle'></i><strong> {{trans('general.no_annotators')}}</strong>
                        </div>
                        @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class='text-center'>{{trans('general.top_annotators_rank')}}</th>
                                    <th>{{trans('general.top_annotators_name')}}</th>
                                    <th class='text-center' data-toggle="tooltip" data-placement="bottom" title="{{trans('general.top_annotators_amount_tooltip')}}">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($topAnnotators as $annotator)
                                <tr class='{{$loop->index < 3 ? "bg-success":""}}'>
                                    <td class='text-center'>{{$loop->iteration}}º</td>
                                    <td>
                                        {{explode(' ', $annotator['name'])[0]}}
                                        @if(!Auth::guest() && $annotator['id'] === Auth::user()->id)
                                        <span class='label label-success'>{{trans('general.you_indicator')}}</span>
                                        @endif
                                    </td>
                                    <td class='text-center'>
                                        <span class='label {{$loop->index < 3 ? "label-success":"label-default"}}'>{{$annotator['annotations_count']}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="service">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-12">
                <h3>{{mb_strtoupper(trans('general.annotations_over_time'))}}</h3>
                <h6 id='annotationPeriod'></h6>
            </div>
            <div class="col-lg-12 hidden-xs">
                <canvas id="annotationsChart"></canvas>
            </div>
            <div class="col-lg-12 hidden-lg hidden-md hidden-sm">
                <canvas id="annotationsChartXS" height="400"></canvas>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-scripts')
<script src="{{asset('js/Chart.bundle.min.js')}}"></script>
<script>
    var videosLabel = "{!!ucwords(trans_choice('general.video', $readyVideos))!!}";
    var evaluatedLabel = "{!!ucwords(trans_choice('general.videos_evaluated',0))!!}";
    var annotationsVideosLabel = "{!!trans('general.annotation_performed')!!}";
    var lastEvaluationLabel = "{!!trans('general.last_evaluation')!!}";
    var byLabel = "{!!trans('general.by')!!}";
    var locale = "{!!app()->getLocale()!!}";

    var amountVideosReady = {!!$readyVideos!!};
    var amountVideosFullyAnnotated = {!!$fullyAnnotatedVideos!!};
    var annotationsDataObject = JSON.parse('{!!json_encode($annotationsByDay)!!}');
    var annotationsData = Object.keys(annotationsDataObject).map(function (key) {
        var value = annotationsDataObject[key];
        value.annotation_date = key;
        return value;
    });

    setupAnnotatioPeriodLabel(annotationsData);

    var progressChart = new Chart($('#progressChart'), {
        type: 'bar',
        data: {
            datasets: [{
                    data: [amountVideosFullyAnnotated],
                    backgroundColor: ['#5cb85c'],
                }],
            labels: [
                evaluatedLabel,
            ],
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                        gridLines: {
                            color: '#fff',
                            zeroLineColor: '#fff',
                        },
                        ticks: {
                            beginAtZero: true,
                            fontColor: '#fff'
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            color: '#fff',
                            zeroLineColor: '#fff',

                        },
                        ticks: {
                            beginAtZero: true,
                            max: amountVideosReady,
                            fontColor: '#fff'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: videosLabel,
                            fontColor: '#fff'
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var percent = (data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] / amountVideosReady) * 100;
                        percent = percent.toFixed(2);
                        return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + ' (' + percent + '%)';
                    }
                }
            }
        }
    });

    var annotationsChartData = {
        type: 'line',
        data: {
            labels: annotationsData.map(function (element) {
                var parts = element.annotation_date.split('-');
                if (locale === 'en') {
                    return parts[1] + '/' + parts[2];
                } else {
                    return parts[2] + '/' + parts[1];
                }
            }),
            datasets: [
                {
                    data: annotationsData.map(x => x.all.counter),
                    borderColor: '#00b3fe',
                    label: annotationsVideosLabel,
                    fill: false,
                    lineTension: 0,
                    pointHitRadius: 10,
                },
                {
                    data: annotationsData.map(x => x.videosFullyAnnotated),
                    borderColor: '#5cb85c',
                    label: lastEvaluationLabel,
                    fill: false,
                    lineTension: 0,
                    pointHitRadius: 10,
                },
            ]
        },
        options: {
            tooltips: {
                callbacks: {
                    afterLabel: function (tooltipItem, data) {
                        if(tooltipItem.datasetIndex === 0){
                            return byLabel + ':\n' + annotationsData[tooltipItem.index].all.annotators.map(x => '- ' + x.name.split(' ')[0] + ': ' + x.counter).join('\n');
                        }
                    }
                }
            }
        }
    };

    var annotationsChart = new Chart($('#annotationsChart'), annotationsChartData);
    var annotationsChartXS = new Chart($('#annotationsChartXS'), annotationsChartData);

    function setupAnnotatioPeriodLabel(annotationsData) {
        var period = Array(2);//[0] earliest date, [1] latest date
        period.fill(annotationsData[0] ? annotationsData[0].annotation_date : '');
        annotationsData.forEach(function (element) {
            if (element.annotation_date.localeCompare(period[0]) < 0) {
                period[0] = element.annotation_date;
            } else if (element.annotation_date.localeCompare(period[1]) > 0) {
                period[1] = element.annotation_date;
            }
            ;
        });
        var dateParts;
        period.forEach(function (date, index) {
            if (date !== '') {
                dateParts = date.split('-');
                if (locale === 'en') {
                    period[index] = dateParts[1] + '/' + dateParts[2] + '/' + dateParts[0];
                } else {
                    period[index] = dateParts[2] + '/' + dateParts[1] + '/' + dateParts[0];
                }
            }
        });
        period = period.filter(date => date.length > 0);
        $('#annotationPeriod').text(period.join(' - '));
    }
</script>
@endsection