<?php

return [
    '404' => 'Page Not Found',
    '500' => 'Internal Server Error',
];
