#!/usr/bin/python


'''
This script is responsible for updating the downloaded status of the videos in the database.
'''
import sys
import os
import glob
import mysql.connector
import json
import dateutil.parser
import datetime

if not os.path.abspath('../') in sys.path:
	sys.path.append(os.path.abspath('../'))

import utils as db_util
import vhd_utils as util


util.LOG_LEVEL = util.LOG_INFO


def get_videos_list():
	'''
	Gets the list of videos in the database
	'''
	mydb = db_util.get_connector()
	cursor = mydb.cursor()

	sql = 'SELECT id FROM yt_video_snippets'
	cursor.execute(sql)
	videos_ids = cursor.fetchall()
	return videos_ids

def update_download_status(status_tuples):
	'''
	Updates the downloaded status for a set of videos in the database.
	'''
	mydb = db_util.get_connector()
	cursor = mydb.cursor()

	sql = ('UPDATE yt_video_snippets '
		+ 'SET downloaded = %s '
		+ 'WHERE id = %s')
	
	results = []
	for status_tuple in status_tuples:
		try:
			results.append(db_util.perform_update(sql, status_tuple))
		except Exception as e:
			util.log_error('Error: ' + str(e))
			results.append({
				'status': 3,
				'message': str(e)
			})

	return results


if __name__ == '__main__':
	startTime = datetime.datetime.now()
	util.log_info('Start time: ' + str(startTime))

	if len(sys.argv) < 2:
		print 'ERROR: You must provide the folder where the downloaded videos are.'
		print '\nUsage:'
		print '  python update_downloaded_status.py videos_folder\n'
	else:
		folder = sys.argv[1]
		videos_ids = get_videos_list()
		download_status = []
		for video_id in videos_ids:
			video_found = len(glob.glob(folder + video_id[0] + '*')) > 0
			download_status.append((video_found, video_id[0]))

		util.save_json_list(download_status, 'hi.log')
		results = update_download_status(download_status)
		util.save_json_list(results, 'udpdate_downloaded_status_results.json')

	endTime = datetime.datetime.now()
	util.log_info('End time: ' + str(endTime))
	util.log_info('Elapsed time: ' + str(endTime - startTime))
