@extends('layouts.contentpage')

@section('title')
{{trans('auth.register')}}
@endsection

@section('content')
<div class="container mtb">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form id='registrationForm' class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">{{trans('auth.name_field')}}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">{{trans('auth.email_field')}}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">{{trans('auth.password_field')}}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">{{trans('auth.password_confirmation_field')}}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group" id='recaptchaFormGroup'>
                    <div class="g-recaptcha col-md-offset-4" data-sitekey="6LfV-3gUAAAAALSYpGXZsxBTDZH69C92X4Sriz_5" data-callback='recaptchaChecked'></div>
                    <div class='col-md-offset-4 help-block hidden'>{{trans('auth.recaptcha_unchecked')}}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-theme">
                            {{trans('auth.register')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class='col-md-8 col-md-offset-2 text-center'>
            <h4>{{trans('auth.login_alternative')}}</h4>
        </div>
    </div>
    @if ($errors->has('social'))
    <div class="row">
        <div class='col-md-6 col-md-offset-3 col-sm-12'>
            <div class="alert alert-danger" role="alert">
                <i class='fa fa-exclamation-triangle'></i><strong> {{trans('general.annotation_error_title')}}</strong><br/>
                {{ $errors->first('social') }}
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class='col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3'>
            <a class='btn btn-danger btn-lg btn-block' href='{{Helpers::url('login/google')}}'>
                <i class='fa fa-google-plus-square'></i> Google
            </a>
            <p class='hidden-lg hidden-md hidden-sm'></p>
        </div>
        {{--<div class='col-md-3 col-sm-6'>
            <a class='btn btn-primary btn-lg btn-block' href='{{Helpers::url('login/facebook')}}'>
                <i class='fa fa-facebook-square'></i> Facebook
            </a>
        </div>--}}
    </div>
</div>
@endsection

@section('page-scripts')
<script src='https://www.google.com/recaptcha/api.js?hl={{app()->getLocale()}}&onload=onRecaptchaLoad'></script>
<script>
var recaptchaReady = false;
var form = $('#registrationForm');

window.onload = function () {
    form.submit(submitRegistration);

};

function recaptchaChecked(){
    console.log('hi');
    $('#recaptchaFormGroup').removeClass('has-error');
    $('#recaptchaFormGroup').find('.help-block').addClass('hidden');
}
function submitRegistration(event) {
    if (!recaptchaReady) {
        event.preventDefault();
        $('#recaptchaFormGroup').addClass('has-error');
        $('#recaptchaFormGroup').find('.help-block').removeClass('hidden');
        return;
    }
    var recaptchaResponse = grecaptcha.getResponse();
    if (recaptchaResponse === '') {
        event.preventDefault();
        $('#recaptchaFormGroup').addClass('has-error');
        $('#recaptchaFormGroup').find('.help-block').removeClass('hidden');
        return;
    }
}

function onRecaptchaLoad() {
    console.log('grecaptcha is ready!');
    recaptchaReady = true;
}


</script>
@endsection