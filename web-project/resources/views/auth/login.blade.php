@extends('layouts.contentpage')

@section('title')
{{trans('auth.login')}}
@endsection

@section('content')
<div class="container mtb">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">{{trans('auth.email_field')}}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">{{trans('auth.password_field')}}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{--<div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{trans('auth.remember_me')}}
                            </label>
                        </div>
                    </div>
                </div>--}}

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-theme">
                            {{trans('auth.login')}}
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{trans('auth.forgot_password')}}
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class='col-md-8 col-md-offset-2 text-center'>
            <h4>{{trans('auth.login_alternative')}}</h4>
        </div>
    </div>
    @if ($errors->has('social'))
    <div class="row">
        <div class='col-md-6 col-md-offset-3 col-sm-12'>
            <div class="alert alert-danger" role="alert">
                <i class='fa fa-exclamation-triangle'></i><strong> {{trans('general.annotation_error_title')}}</strong><br/>
                {{ $errors->first('social') }}
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class='col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3'>
            <a class='btn btn-danger btn-lg btn-block' href='{{Helpers::url('login/google')}}'>
                <i class='fa fa-google-plus-square'></i> Google
            </a>
            <p class='hidden-lg hidden-md hidden-sm'></p>
        </div>
        {{--<div class='col-md-3 col-sm-6'>
            <a class='btn btn-primary btn-lg btn-block' href='{{Helpers::url('login/facebook')}}'>
                <i class='fa fa-facebook-square'></i> Facebook
            </a>
        </div>--}}
    </div>
</div>
@endsection

@section('page-scripts')
@endsection